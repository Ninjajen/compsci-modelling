// This file implements a module where we define the data types
// to represent the different types of expressions
module PenguinTypesAST

type cmd =
  | Assign of (string * aexpr)
  | AssignArray of (string * aexpr * aexpr)
  | Statements of (cmd * cmd)
  | ConditionalStatement of gcmd
  | ControlFlowStatement of gcmd
  | Skip
and gcmd =
  | If of (bexpr * cmd)
  | ElseIf of (gcmd * gcmd)
and aexpr =
  | Num of int
  | TimesExpr of (aexpr * aexpr)
  | DivExpr of (aexpr * aexpr)
  | PlusExpr of (aexpr * aexpr)
  | MinusExpr of (aexpr * aexpr)
  | PowExpr of (aexpr * aexpr)
  | UPlusExpr of (aexpr)
  | UMinusExpr of (aexpr)
  | Array of (string * aexpr)
  | Var of string
and bexpr =
  | Bool of bool
  | OrExpr of (bexpr * bexpr)
  | AndExpr of (bexpr * bexpr)
  | OrFullExpr of (bexpr * bexpr)
  | AndFullExpr of (bexpr * bexpr)
  | NotExpr of (bexpr)
  | EqualExpr of (aexpr * aexpr)
  | NotEqualExpr of (aexpr * aexpr)
  | GreaterExpr of (aexpr * aexpr)
  | GreaterEqualExpr of (aexpr * aexpr)
  | LesserExpr of (aexpr * aexpr)
  | LesserEqualExpr of (aexpr * aexpr)