#r "nuget: FsLexYacc.Runtime"
open FSharp.Text.Lexing
open System
#load "PenguinTypesAST.fs"
open PenguinTypesAST
#load "PenguinParser.fs"
open PenguinParser
#load "PenguinLexer.fs"
open PenguinLexer

// Recursive definition of the evaluation function for arithmetic expressions
let rec evala e =
  match e with
    | Num(x)            -> (string) x
    | UMinusExpr(x)     -> "-" + evala(x)
    | PowExpr(x,y)      -> evala(x) + "^" + evala (y)
    | TimesExpr(x,y)    -> evala(x) + " * " + evala (y)
    | DivExpr(x,y)      -> evala(x) + " / " + evala (y)
    | PlusExpr(x,y)     -> evala(x) + " + " + evala (y)
    | MinusExpr(x,y)    -> evala(x) + " - " + evala (y)
    | UPlusExpr(x)      -> evala(x)
    | Array(x, y)       -> x + "[" + evala(y) + "]"
    | Var(x)            -> x
// Recursive definition of the evaluation function for Boolean expressions
and evalb e =
    match e with
    | Bool(x)               -> (string) x
    | OrExpr(x, y)          -> evalb(x)  + " || " + evalb(y)
    | AndExpr(x, y)         -> evalb(x)  + " && " + evalb(y)
    | OrFullExpr(x, y)      -> evalb(x)  + " | "  + evalb(y)
    | AndFullExpr(x, y)     -> evalb(x)  + " & "  + evalb(y)
    | NotExpr(x)            -> "!" + evalb(x)
    | EqualExpr(x, y)       -> evala(x) + " = " + evala(y)
    | NotEqualExpr(x, y)    -> evala(x) + " != " + evala(y)
    | GreaterExpr(x, y)     -> evala(x) + " > " + evala(y)
    | GreaterEqualExpr(x, y)-> evala(x) + " >= " + evala(y)
    | LesserExpr(x, y)      -> evala(x) + " < " + evala(y)
    | LesserEqualExpr(x, y) -> evala(x) + " <= " + evala(y)
// Recursive definition of the evaluation function for Commands
and evalCMD e =
    match e with
    | Assign(x,y)               -> x + " := " + evala(y)
    | AssignArray(x, y, z)      -> x + "[" + evala(y) + "] := " + evala(z)
    | Skip                      -> "skip"
    | Statements(x, y)          -> evalCMD(x) + " ; " + evalCMD(y)
    | ConditionalStatement(x)   -> "if " + evalGCMD(x) + " fi"
    | ControlFlowStatement(x)   -> "do " + evalGCMD(x) + " od"
// Recursive definition of the evaluation function for Guarded Commands
and evalGCMD e =
    match e with
    | If(x, y)                  -> evalb(x) + " -> " + evalCMD(y)
    | ElseIf(x, y)              -> evalGCMD(x) + " [] " + evalGCMD(y)

let parse input =
    // translate string into a buffer of characters
    let lexbuf = LexBuffer<char>.FromString input
    // translate the buffer into a stream of tokens and parse them
    let res = PenguinParser.start PenguinLexer.tokenize lexbuf
    // return the result of parsing (i.e. value of type "expr")
    res

// Function that interacts with the user
let rec compute n =
    if n = 0 then
        printfn "Bye bye"
    else
        printf "Enter an expression: "
        try
        // We parse the input string
        let e = parse (Console.ReadLine())
        // and print the result of evaluating it
        printfn "Result: %s" (evalCMD(e))
        compute n
        with err -> compute (n-1)

// Interact with user
compute 3