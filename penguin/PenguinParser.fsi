// Signature file for parser generated by fsyacc
module PenguinParser
type token = 
  | ASSIGN
  | SKIP
  | IF
  | FI
  | THEN
  | ELSEIF
  | DO
  | OD
  | SEPARATOR
  | OR
  | AND
  | ORFULL
  | ANDFULL
  | NOT
  | EQUAL
  | NOTEQUAL
  | GREATER
  | GREATEREQUAL
  | LESSER
  | LESSEREQUAL
  | EOF
  | TIMES
  | DIV
  | PLUS
  | MINUS
  | POW
  | LPAR
  | RPAR
  | LBRKT
  | RBRKT
  | VAR of (string)
  | BOOL of (bool)
  | NUM of (int)
type tokenId = 
    | TOKEN_ASSIGN
    | TOKEN_SKIP
    | TOKEN_IF
    | TOKEN_FI
    | TOKEN_THEN
    | TOKEN_ELSEIF
    | TOKEN_DO
    | TOKEN_OD
    | TOKEN_SEPARATOR
    | TOKEN_OR
    | TOKEN_AND
    | TOKEN_ORFULL
    | TOKEN_ANDFULL
    | TOKEN_NOT
    | TOKEN_EQUAL
    | TOKEN_NOTEQUAL
    | TOKEN_GREATER
    | TOKEN_GREATEREQUAL
    | TOKEN_LESSER
    | TOKEN_LESSEREQUAL
    | TOKEN_EOF
    | TOKEN_TIMES
    | TOKEN_DIV
    | TOKEN_PLUS
    | TOKEN_MINUS
    | TOKEN_POW
    | TOKEN_LPAR
    | TOKEN_RPAR
    | TOKEN_LBRKT
    | TOKEN_RBRKT
    | TOKEN_VAR
    | TOKEN_BOOL
    | TOKEN_NUM
    | TOKEN_end_of_input
    | TOKEN_error
type nonTerminalId = 
    | NONTERM__startstart
    | NONTERM_start
    | NONTERM_command
    | NONTERM_gCommand
    | NONTERM_arithmeticExpression
    | NONTERM_booleanExpression
/// This function maps tokens to integer indexes
val tagOfToken: token -> int

/// This function maps integer indexes to symbolic token ids
val tokenTagToTokenId: int -> tokenId

/// This function maps production indexes returned in syntax errors to strings representing the non terminal that would be produced by that production
val prodIdxToNonTerminal: int -> nonTerminalId

/// This function gets the name of a token as a string
val token_to_string: token -> string
val start : (FSharp.Text.Lexing.LexBuffer<'cty> -> token) -> FSharp.Text.Lexing.LexBuffer<'cty> -> (cmd) 
