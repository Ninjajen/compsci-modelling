// Implementation file for parser generated by fsyacc
module PenguinParser
#nowarn "64";; // turn off warnings that type variables used in production annotations are instantiated to concrete type
open FSharp.Text.Lexing
open FSharp.Text.Parsing.ParseHelpers
# 2 "PenguinParser.fsp"

open PenguinTypesAST

# 10 "PenguinParser.fs"
// This type is the type of tokens accepted by the parser
type token = 
  | ASSIGN
  | SKIP
  | IF
  | FI
  | THEN
  | ELSEIF
  | DO
  | OD
  | SEPARATOR
  | OR
  | AND
  | ORFULL
  | ANDFULL
  | NOT
  | EQUAL
  | NOTEQUAL
  | GREATER
  | GREATEREQUAL
  | LESSER
  | LESSEREQUAL
  | EOF
  | TIMES
  | DIV
  | PLUS
  | MINUS
  | POW
  | LPAR
  | RPAR
  | LBRKT
  | RBRKT
  | VAR of (string)
  | BOOL of (bool)
  | NUM of (int)
// This type is used to give symbolic names to token indexes, useful for error messages
type tokenId = 
    | TOKEN_ASSIGN
    | TOKEN_SKIP
    | TOKEN_IF
    | TOKEN_FI
    | TOKEN_THEN
    | TOKEN_ELSEIF
    | TOKEN_DO
    | TOKEN_OD
    | TOKEN_SEPARATOR
    | TOKEN_OR
    | TOKEN_AND
    | TOKEN_ORFULL
    | TOKEN_ANDFULL
    | TOKEN_NOT
    | TOKEN_EQUAL
    | TOKEN_NOTEQUAL
    | TOKEN_GREATER
    | TOKEN_GREATEREQUAL
    | TOKEN_LESSER
    | TOKEN_LESSEREQUAL
    | TOKEN_EOF
    | TOKEN_TIMES
    | TOKEN_DIV
    | TOKEN_PLUS
    | TOKEN_MINUS
    | TOKEN_POW
    | TOKEN_LPAR
    | TOKEN_RPAR
    | TOKEN_LBRKT
    | TOKEN_RBRKT
    | TOKEN_VAR
    | TOKEN_BOOL
    | TOKEN_NUM
    | TOKEN_end_of_input
    | TOKEN_error
// This type is used to give symbolic names to token indexes, useful for error messages
type nonTerminalId = 
    | NONTERM__startstart
    | NONTERM_start
    | NONTERM_command
    | NONTERM_gCommand
    | NONTERM_arithmeticExpression
    | NONTERM_booleanExpression

// This function maps tokens to integer indexes
let tagOfToken (t:token) = 
  match t with
  | ASSIGN  -> 0 
  | SKIP  -> 1 
  | IF  -> 2 
  | FI  -> 3 
  | THEN  -> 4 
  | ELSEIF  -> 5 
  | DO  -> 6 
  | OD  -> 7 
  | SEPARATOR  -> 8 
  | OR  -> 9 
  | AND  -> 10 
  | ORFULL  -> 11 
  | ANDFULL  -> 12 
  | NOT  -> 13 
  | EQUAL  -> 14 
  | NOTEQUAL  -> 15 
  | GREATER  -> 16 
  | GREATEREQUAL  -> 17 
  | LESSER  -> 18 
  | LESSEREQUAL  -> 19 
  | EOF  -> 20 
  | TIMES  -> 21 
  | DIV  -> 22 
  | PLUS  -> 23 
  | MINUS  -> 24 
  | POW  -> 25 
  | LPAR  -> 26 
  | RPAR  -> 27 
  | LBRKT  -> 28 
  | RBRKT  -> 29 
  | VAR _ -> 30 
  | BOOL _ -> 31 
  | NUM _ -> 32 

// This function maps integer indexes to symbolic token ids
let tokenTagToTokenId (tokenIdx:int) = 
  match tokenIdx with
  | 0 -> TOKEN_ASSIGN 
  | 1 -> TOKEN_SKIP 
  | 2 -> TOKEN_IF 
  | 3 -> TOKEN_FI 
  | 4 -> TOKEN_THEN 
  | 5 -> TOKEN_ELSEIF 
  | 6 -> TOKEN_DO 
  | 7 -> TOKEN_OD 
  | 8 -> TOKEN_SEPARATOR 
  | 9 -> TOKEN_OR 
  | 10 -> TOKEN_AND 
  | 11 -> TOKEN_ORFULL 
  | 12 -> TOKEN_ANDFULL 
  | 13 -> TOKEN_NOT 
  | 14 -> TOKEN_EQUAL 
  | 15 -> TOKEN_NOTEQUAL 
  | 16 -> TOKEN_GREATER 
  | 17 -> TOKEN_GREATEREQUAL 
  | 18 -> TOKEN_LESSER 
  | 19 -> TOKEN_LESSEREQUAL 
  | 20 -> TOKEN_EOF 
  | 21 -> TOKEN_TIMES 
  | 22 -> TOKEN_DIV 
  | 23 -> TOKEN_PLUS 
  | 24 -> TOKEN_MINUS 
  | 25 -> TOKEN_POW 
  | 26 -> TOKEN_LPAR 
  | 27 -> TOKEN_RPAR 
  | 28 -> TOKEN_LBRKT 
  | 29 -> TOKEN_RBRKT 
  | 30 -> TOKEN_VAR 
  | 31 -> TOKEN_BOOL 
  | 32 -> TOKEN_NUM 
  | 35 -> TOKEN_end_of_input
  | 33 -> TOKEN_error
  | _ -> failwith "tokenTagToTokenId: bad token"

/// This function maps production indexes returned in syntax errors to strings representing the non terminal that would be produced by that production
let prodIdxToNonTerminal (prodIdx:int) = 
  match prodIdx with
    | 0 -> NONTERM__startstart 
    | 1 -> NONTERM_start 
    | 2 -> NONTERM_command 
    | 3 -> NONTERM_command 
    | 4 -> NONTERM_command 
    | 5 -> NONTERM_command 
    | 6 -> NONTERM_command 
    | 7 -> NONTERM_command 
    | 8 -> NONTERM_gCommand 
    | 9 -> NONTERM_gCommand 
    | 10 -> NONTERM_arithmeticExpression 
    | 11 -> NONTERM_arithmeticExpression 
    | 12 -> NONTERM_arithmeticExpression 
    | 13 -> NONTERM_arithmeticExpression 
    | 14 -> NONTERM_arithmeticExpression 
    | 15 -> NONTERM_arithmeticExpression 
    | 16 -> NONTERM_arithmeticExpression 
    | 17 -> NONTERM_arithmeticExpression 
    | 18 -> NONTERM_arithmeticExpression 
    | 19 -> NONTERM_arithmeticExpression 
    | 20 -> NONTERM_arithmeticExpression 
    | 21 -> NONTERM_booleanExpression 
    | 22 -> NONTERM_booleanExpression 
    | 23 -> NONTERM_booleanExpression 
    | 24 -> NONTERM_booleanExpression 
    | 25 -> NONTERM_booleanExpression 
    | 26 -> NONTERM_booleanExpression 
    | 27 -> NONTERM_booleanExpression 
    | 28 -> NONTERM_booleanExpression 
    | 29 -> NONTERM_booleanExpression 
    | 30 -> NONTERM_booleanExpression 
    | 31 -> NONTERM_booleanExpression 
    | 32 -> NONTERM_booleanExpression 
    | 33 -> NONTERM_booleanExpression 
    | _ -> failwith "prodIdxToNonTerminal: bad production index"

let _fsyacc_endOfInputTag = 35 
let _fsyacc_tagOfErrorTerminal = 33

// This function gets the name of a token as a string
let token_to_string (t:token) = 
  match t with 
  | ASSIGN  -> "ASSIGN" 
  | SKIP  -> "SKIP" 
  | IF  -> "IF" 
  | FI  -> "FI" 
  | THEN  -> "THEN" 
  | ELSEIF  -> "ELSEIF" 
  | DO  -> "DO" 
  | OD  -> "OD" 
  | SEPARATOR  -> "SEPARATOR" 
  | OR  -> "OR" 
  | AND  -> "AND" 
  | ORFULL  -> "ORFULL" 
  | ANDFULL  -> "ANDFULL" 
  | NOT  -> "NOT" 
  | EQUAL  -> "EQUAL" 
  | NOTEQUAL  -> "NOTEQUAL" 
  | GREATER  -> "GREATER" 
  | GREATEREQUAL  -> "GREATEREQUAL" 
  | LESSER  -> "LESSER" 
  | LESSEREQUAL  -> "LESSEREQUAL" 
  | EOF  -> "EOF" 
  | TIMES  -> "TIMES" 
  | DIV  -> "DIV" 
  | PLUS  -> "PLUS" 
  | MINUS  -> "MINUS" 
  | POW  -> "POW" 
  | LPAR  -> "LPAR" 
  | RPAR  -> "RPAR" 
  | LBRKT  -> "LBRKT" 
  | RBRKT  -> "RBRKT" 
  | VAR _ -> "VAR" 
  | BOOL _ -> "BOOL" 
  | NUM _ -> "NUM" 

// This function gets the data carried by a token as an object
let _fsyacc_dataOfToken (t:token) = 
  match t with 
  | ASSIGN  -> (null : System.Object) 
  | SKIP  -> (null : System.Object) 
  | IF  -> (null : System.Object) 
  | FI  -> (null : System.Object) 
  | THEN  -> (null : System.Object) 
  | ELSEIF  -> (null : System.Object) 
  | DO  -> (null : System.Object) 
  | OD  -> (null : System.Object) 
  | SEPARATOR  -> (null : System.Object) 
  | OR  -> (null : System.Object) 
  | AND  -> (null : System.Object) 
  | ORFULL  -> (null : System.Object) 
  | ANDFULL  -> (null : System.Object) 
  | NOT  -> (null : System.Object) 
  | EQUAL  -> (null : System.Object) 
  | NOTEQUAL  -> (null : System.Object) 
  | GREATER  -> (null : System.Object) 
  | GREATEREQUAL  -> (null : System.Object) 
  | LESSER  -> (null : System.Object) 
  | LESSEREQUAL  -> (null : System.Object) 
  | EOF  -> (null : System.Object) 
  | TIMES  -> (null : System.Object) 
  | DIV  -> (null : System.Object) 
  | PLUS  -> (null : System.Object) 
  | MINUS  -> (null : System.Object) 
  | POW  -> (null : System.Object) 
  | LPAR  -> (null : System.Object) 
  | RPAR  -> (null : System.Object) 
  | LBRKT  -> (null : System.Object) 
  | RBRKT  -> (null : System.Object) 
  | VAR _fsyacc_x -> Microsoft.FSharp.Core.Operators.box _fsyacc_x 
  | BOOL _fsyacc_x -> Microsoft.FSharp.Core.Operators.box _fsyacc_x 
  | NUM _fsyacc_x -> Microsoft.FSharp.Core.Operators.box _fsyacc_x 
let _fsyacc_gotos = [| 0us; 65535us; 1us; 65535us; 0us; 1us; 3us; 65535us; 0us; 2us; 15us; 13us; 23us; 14us; 3us; 65535us; 16us; 17us; 19us; 20us; 25us; 24us; 27us; 65535us; 5us; 6us; 7us; 8us; 10us; 11us; 16us; 40us; 19us; 40us; 25us; 40us; 28us; 29us; 47us; 31us; 48us; 32us; 49us; 33us; 50us; 34us; 51us; 35us; 52us; 36us; 53us; 37us; 54us; 38us; 55us; 39us; 64us; 40us; 65us; 40us; 66us; 40us; 67us; 40us; 68us; 40us; 69us; 41us; 70us; 42us; 71us; 43us; 72us; 44us; 73us; 45us; 74us; 46us; 9us; 65535us; 16us; 22us; 19us; 22us; 25us; 22us; 55us; 63us; 64us; 58us; 65us; 59us; 66us; 60us; 67us; 61us; 68us; 62us; |]
let _fsyacc_sparseGotoTableRowOffsets = [|0us; 1us; 3us; 7us; 11us; 39us; |]
let _fsyacc_stateToProdIdxsTableElements = [| 1us; 0us; 1us; 0us; 2us; 1us; 5us; 1us; 1us; 2us; 2us; 3us; 1us; 2us; 6us; 2us; 12us; 13us; 14us; 15us; 16us; 1us; 3us; 6us; 3us; 12us; 13us; 14us; 15us; 16us; 1us; 3us; 1us; 3us; 6us; 3us; 12us; 13us; 14us; 15us; 16us; 1us; 4us; 2us; 5us; 5us; 2us; 5us; 8us; 1us; 5us; 1us; 6us; 2us; 6us; 9us; 1us; 6us; 1us; 7us; 2us; 7us; 9us; 1us; 7us; 5us; 8us; 22us; 23us; 24us; 25us; 1us; 8us; 2us; 9us; 9us; 1us; 9us; 1us; 10us; 2us; 11us; 20us; 1us; 11us; 6us; 11us; 12us; 13us; 14us; 15us; 16us; 1us; 11us; 6us; 12us; 12us; 13us; 14us; 15us; 16us; 6us; 12us; 13us; 13us; 14us; 15us; 16us; 6us; 12us; 13us; 14us; 14us; 15us; 16us; 6us; 12us; 13us; 14us; 15us; 15us; 16us; 6us; 12us; 13us; 14us; 15us; 16us; 16us; 6us; 12us; 13us; 14us; 15us; 16us; 17us; 6us; 12us; 13us; 14us; 15us; 16us; 18us; 6us; 12us; 13us; 14us; 15us; 16us; 19us; 12us; 12us; 13us; 14us; 15us; 16us; 19us; 27us; 28us; 29us; 30us; 31us; 32us; 11us; 12us; 13us; 14us; 15us; 16us; 27us; 28us; 29us; 30us; 31us; 32us; 6us; 12us; 13us; 14us; 15us; 16us; 27us; 6us; 12us; 13us; 14us; 15us; 16us; 28us; 6us; 12us; 13us; 14us; 15us; 16us; 29us; 6us; 12us; 13us; 14us; 15us; 16us; 30us; 6us; 12us; 13us; 14us; 15us; 16us; 31us; 6us; 12us; 13us; 14us; 15us; 16us; 32us; 1us; 12us; 1us; 13us; 1us; 14us; 1us; 15us; 1us; 16us; 1us; 17us; 1us; 18us; 1us; 19us; 2us; 19us; 33us; 1us; 19us; 1us; 21us; 5us; 22us; 22us; 23us; 24us; 25us; 5us; 22us; 23us; 23us; 24us; 25us; 5us; 22us; 23us; 24us; 24us; 25us; 5us; 22us; 23us; 24us; 25us; 25us; 5us; 22us; 23us; 24us; 25us; 26us; 5us; 22us; 23us; 24us; 25us; 33us; 1us; 22us; 1us; 23us; 1us; 24us; 1us; 25us; 1us; 26us; 1us; 27us; 1us; 28us; 1us; 29us; 1us; 30us; 1us; 31us; 1us; 32us; 1us; 33us; |]
let _fsyacc_stateToProdIdxsTableRowOffsets = [|0us; 2us; 4us; 7us; 9us; 12us; 14us; 21us; 23us; 30us; 32us; 34us; 41us; 43us; 46us; 49us; 51us; 53us; 56us; 58us; 60us; 63us; 65us; 71us; 73us; 76us; 78us; 80us; 83us; 85us; 92us; 94us; 101us; 108us; 115us; 122us; 129us; 136us; 143us; 150us; 163us; 175us; 182us; 189us; 196us; 203us; 210us; 217us; 219us; 221us; 223us; 225us; 227us; 229us; 231us; 233us; 236us; 238us; 240us; 246us; 252us; 258us; 264us; 270us; 276us; 278us; 280us; 282us; 284us; 286us; 288us; 290us; 292us; 294us; 296us; 298us; |]
let _fsyacc_action_rows = 76
let _fsyacc_actionTableElements = [|4us; 32768us; 1us; 12us; 2us; 16us; 6us; 19us; 30us; 4us; 0us; 49152us; 2us; 32768us; 8us; 15us; 20us; 3us; 0us; 16385us; 2us; 32768us; 0us; 5us; 28us; 7us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 16386us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 6us; 32768us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 29us; 9us; 1us; 32768us; 0us; 10us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 16387us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 0us; 16388us; 1us; 16389us; 8us; 15us; 1us; 16392us; 8us; 15us; 4us; 32768us; 1us; 12us; 2us; 16us; 6us; 19us; 30us; 4us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 2us; 32768us; 3us; 18us; 5us; 25us; 0us; 16390us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 2us; 32768us; 5us; 25us; 7us; 21us; 0us; 16391us; 5us; 32768us; 4us; 23us; 9us; 64us; 10us; 65us; 11us; 66us; 12us; 67us; 4us; 32768us; 1us; 12us; 2us; 16us; 6us; 19us; 30us; 4us; 1us; 16393us; 5us; 25us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 0us; 16394us; 1us; 16404us; 28us; 28us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 6us; 32768us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 29us; 30us; 0us; 16395us; 2us; 16396us; 24us; 50us; 25us; 51us; 2us; 16397us; 24us; 50us; 25us; 51us; 4us; 16398us; 21us; 47us; 22us; 48us; 24us; 50us; 25us; 51us; 0us; 16399us; 2us; 16400us; 24us; 50us; 25us; 51us; 4us; 16401us; 21us; 47us; 22us; 48us; 24us; 50us; 25us; 51us; 0us; 16402us; 6us; 32768us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 27us; 56us; 12us; 32768us; 14us; 69us; 15us; 70us; 16us; 71us; 17us; 72us; 18us; 73us; 19us; 74us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 27us; 56us; 11us; 32768us; 14us; 69us; 15us; 70us; 16us; 71us; 17us; 72us; 18us; 73us; 19us; 74us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16411us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16412us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16413us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16414us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16415us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 16416us; 21us; 47us; 22us; 48us; 23us; 49us; 24us; 50us; 25us; 51us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 0us; 16403us; 0us; 16405us; 2us; 16406us; 10us; 65us; 12us; 67us; 0us; 16407us; 2us; 16408us; 10us; 65us; 12us; 67us; 0us; 16409us; 0us; 16410us; 5us; 32768us; 9us; 64us; 10us; 65us; 11us; 66us; 12us; 67us; 27us; 75us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 7us; 32768us; 13us; 68us; 23us; 52us; 24us; 53us; 26us; 55us; 30us; 27us; 31us; 57us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 5us; 32768us; 23us; 52us; 24us; 53us; 26us; 54us; 30us; 27us; 32us; 26us; 0us; 16417us; |]
let _fsyacc_actionTableRowOffsets = [|0us; 5us; 6us; 9us; 10us; 13us; 19us; 25us; 31us; 38us; 40us; 46us; 52us; 53us; 55us; 57us; 62us; 70us; 73us; 74us; 82us; 85us; 86us; 92us; 97us; 99us; 107us; 108us; 110us; 116us; 123us; 124us; 127us; 130us; 135us; 136us; 139us; 144us; 145us; 152us; 165us; 177us; 183us; 189us; 195us; 201us; 207us; 213us; 219us; 225us; 231us; 237us; 243us; 249us; 255us; 261us; 269us; 270us; 271us; 274us; 275us; 278us; 279us; 280us; 286us; 294us; 302us; 310us; 318us; 326us; 332us; 338us; 344us; 350us; 356us; 362us; |]
let _fsyacc_reductionSymbolCounts = [|1us; 2us; 3us; 6us; 1us; 3us; 3us; 3us; 3us; 3us; 1us; 4us; 3us; 3us; 3us; 3us; 3us; 2us; 2us; 3us; 1us; 1us; 3us; 3us; 3us; 3us; 2us; 3us; 3us; 3us; 3us; 3us; 3us; 3us; |]
let _fsyacc_productionToNonTerminalTable = [|0us; 1us; 2us; 2us; 2us; 2us; 2us; 2us; 3us; 3us; 4us; 4us; 4us; 4us; 4us; 4us; 4us; 4us; 4us; 4us; 4us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; 5us; |]
let _fsyacc_immediateActions = [|65535us; 49152us; 65535us; 16385us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 16388us; 65535us; 65535us; 65535us; 65535us; 65535us; 16390us; 65535us; 65535us; 16391us; 65535us; 65535us; 65535us; 65535us; 16394us; 65535us; 65535us; 65535us; 16395us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 16403us; 16405us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 65535us; 16417us; |]
let _fsyacc_reductions ()  =    [| 
# 295 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : cmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
                      raise (FSharp.Text.Parsing.Accept(Microsoft.FSharp.Core.Operators.box _1))
                   )
                 : '_startstart));
# 304 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : cmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 40 "PenguinParser.fsp"
                                                      _1 
                   )
# 40 "PenguinParser.fsp"
                 : cmd));
# 315 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : string)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 50 "PenguinParser.fsp"
                                                                                               Assign(_1,_3) 
                   )
# 50 "PenguinParser.fsp"
                 : cmd));
# 327 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : string)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _6 = (let data = parseState.GetInput(6) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 51 "PenguinParser.fsp"
                                                                                               AssignArray(_1,_3,_6) 
                   )
# 51 "PenguinParser.fsp"
                 : cmd));
# 340 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 52 "PenguinParser.fsp"
                                                                                               Skip 
                   )
# 52 "PenguinParser.fsp"
                 : cmd));
# 350 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : cmd)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : cmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 53 "PenguinParser.fsp"
                                                                                               Statements(_1, _3) 
                   )
# 53 "PenguinParser.fsp"
                 : cmd));
# 362 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : gcmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 54 "PenguinParser.fsp"
                                                                                               ConditionalStatement(_2) 
                   )
# 54 "PenguinParser.fsp"
                 : cmd));
# 373 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : gcmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 55 "PenguinParser.fsp"
                                                                                               ControlFlowStatement(_2) 
                   )
# 55 "PenguinParser.fsp"
                 : cmd));
# 384 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : cmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 58 "PenguinParser.fsp"
                                                                                               If(_1, _3) 
                   )
# 58 "PenguinParser.fsp"
                 : gcmd));
# 396 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : gcmd)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : gcmd)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 59 "PenguinParser.fsp"
                                                                                               ElseIf(_1, _3) 
                   )
# 59 "PenguinParser.fsp"
                 : gcmd));
# 408 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : int)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 62 "PenguinParser.fsp"
                                                                                               Num(_1) 
                   )
# 62 "PenguinParser.fsp"
                 : aexpr));
# 419 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : string)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 63 "PenguinParser.fsp"
                                                                                               Array(_1,_3) 
                   )
# 63 "PenguinParser.fsp"
                 : aexpr));
# 431 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 64 "PenguinParser.fsp"
                                                                                               TimesExpr(_1,_3) 
                   )
# 64 "PenguinParser.fsp"
                 : aexpr));
# 443 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 65 "PenguinParser.fsp"
                                                                                               DivExpr(_1,_3) 
                   )
# 65 "PenguinParser.fsp"
                 : aexpr));
# 455 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 66 "PenguinParser.fsp"
                                                                                               PlusExpr(_1,_3) 
                   )
# 66 "PenguinParser.fsp"
                 : aexpr));
# 467 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 67 "PenguinParser.fsp"
                                                                                               MinusExpr(_1,_3) 
                   )
# 67 "PenguinParser.fsp"
                 : aexpr));
# 479 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 68 "PenguinParser.fsp"
                                                                                               PowExpr(_1,_3) 
                   )
# 68 "PenguinParser.fsp"
                 : aexpr));
# 491 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 69 "PenguinParser.fsp"
                                                                                               UPlusExpr(_2) 
                   )
# 69 "PenguinParser.fsp"
                 : aexpr));
# 502 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 70 "PenguinParser.fsp"
                                                                                               UMinusExpr(_2) 
                   )
# 70 "PenguinParser.fsp"
                 : aexpr));
# 513 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 71 "PenguinParser.fsp"
                                                                                               _2 
                   )
# 71 "PenguinParser.fsp"
                 : aexpr));
# 524 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : string)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 72 "PenguinParser.fsp"
                                                                                               Var(_1) 
                   )
# 72 "PenguinParser.fsp"
                 : aexpr));
# 535 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bool)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 75 "PenguinParser.fsp"
                                                                                               Bool(_1) 
                   )
# 75 "PenguinParser.fsp"
                 : bexpr));
# 546 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 76 "PenguinParser.fsp"
                                                                                               OrExpr(_1, _3) 
                   )
# 76 "PenguinParser.fsp"
                 : bexpr));
# 558 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 77 "PenguinParser.fsp"
                                                                                               AndExpr(_1, _3) 
                   )
# 77 "PenguinParser.fsp"
                 : bexpr));
# 570 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 78 "PenguinParser.fsp"
                                                                                               OrFullExpr(_1, _3) 
                   )
# 78 "PenguinParser.fsp"
                 : bexpr));
# 582 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 79 "PenguinParser.fsp"
                                                                                               AndFullExpr(_1, _3) 
                   )
# 79 "PenguinParser.fsp"
                 : bexpr));
# 594 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 80 "PenguinParser.fsp"
                                                                                               NotExpr(_2) 
                   )
# 80 "PenguinParser.fsp"
                 : bexpr));
# 605 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 81 "PenguinParser.fsp"
                                                                                             EqualExpr(_1, _3) 
                   )
# 81 "PenguinParser.fsp"
                 : bexpr));
# 617 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 82 "PenguinParser.fsp"
                                                                                               NotEqualExpr(_1, _3) 
                   )
# 82 "PenguinParser.fsp"
                 : bexpr));
# 629 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 83 "PenguinParser.fsp"
                                                                                               GreaterExpr(_1, _3) 
                   )
# 83 "PenguinParser.fsp"
                 : bexpr));
# 641 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 84 "PenguinParser.fsp"
                                                                                               GreaterEqualExpr(_1, _3) 
                   )
# 84 "PenguinParser.fsp"
                 : bexpr));
# 653 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 85 "PenguinParser.fsp"
                                                                                              LesserExpr(_1, _3) 
                   )
# 85 "PenguinParser.fsp"
                 : bexpr));
# 665 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _1 = (let data = parseState.GetInput(1) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            let _3 = (let data = parseState.GetInput(3) in (Microsoft.FSharp.Core.Operators.unbox data : aexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 86 "PenguinParser.fsp"
                                                                                               LesserEqualExpr(_1, _3) 
                   )
# 86 "PenguinParser.fsp"
                 : bexpr));
# 677 "PenguinParser.fs"
        (fun (parseState : FSharp.Text.Parsing.IParseState) ->
            let _2 = (let data = parseState.GetInput(2) in (Microsoft.FSharp.Core.Operators.unbox data : bexpr)) in
            Microsoft.FSharp.Core.Operators.box
                (
                   (
# 87 "PenguinParser.fsp"
                                                                                               _2 
                   )
# 87 "PenguinParser.fsp"
                 : bexpr));
|]
# 689 "PenguinParser.fs"
let tables () : FSharp.Text.Parsing.Tables<_> = 
  { reductions= _fsyacc_reductions ();
    endOfInputTag = _fsyacc_endOfInputTag;
    tagOfToken = tagOfToken;
    dataOfToken = _fsyacc_dataOfToken; 
    actionTableElements = _fsyacc_actionTableElements;
    actionTableRowOffsets = _fsyacc_actionTableRowOffsets;
    stateToProdIdxsTableElements = _fsyacc_stateToProdIdxsTableElements;
    stateToProdIdxsTableRowOffsets = _fsyacc_stateToProdIdxsTableRowOffsets;
    reductionSymbolCounts = _fsyacc_reductionSymbolCounts;
    immediateActions = _fsyacc_immediateActions;
    gotos = _fsyacc_gotos;
    sparseGotoTableRowOffsets = _fsyacc_sparseGotoTableRowOffsets;
    tagOfErrorTerminal = _fsyacc_tagOfErrorTerminal;
    parseError = (fun (ctxt:FSharp.Text.Parsing.ParseErrorContext<_>) -> 
                              match parse_error_rich with 
                              | Some f -> f ctxt
                              | None -> parse_error ctxt.Message);
    numTerminals = 36;
    productionToNonTerminalTable = _fsyacc_productionToNonTerminalTable  }
let engine lexer lexbuf startState = (tables ()).Interpret(lexer, lexbuf, startState)
let start lexer lexbuf : cmd =
    Microsoft.FSharp.Core.Operators.unbox ((tables ()).Interpret(lexer, lexbuf, 0))
