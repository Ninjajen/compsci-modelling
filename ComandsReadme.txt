Building:
1.
../fslexyacc/10.0.0/build/fslex/net46/fslex.exe PenguinLexer.fsl --unicode

2.
../fslexyacc/10.0.0/build/fsyacc/net46/fsyacc.exe PenguinParser.fsp --module PenguinParser

Running:
1. go into Penguin.fsx
2. mark all 'ctrl+A'
3. run code 'alt+enter'